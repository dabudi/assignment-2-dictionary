%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define BUF_LENGTH 256
%define NEXT_ELEM 8

section .rodata
err_long_key: db "Key is too long!", 0
err_not_found: db "No such key found!", 0

section .bss
buffer: resb BUF_LENGTH

section .text
global _start

_start:
    mov rdi, buffer
    mov rsi, BUF_LENGTH
    call read_word
    test rax, rax
    jz .too_long_err 
    push rdx 
    mov rdi, rax 
    mov rsi, head
    call find_word
    pop rdx
    test rax, rax
    jz .not_found_err
    add rax, NEXT_ELEM
    add rax, rdx 
    inc rax 
    mov rdi, rax
    call print_string
    .complete:
        xor rdi, rdi
        call exit
    .too_long_err:
        mov rdi, err_long_key
        jmp .error
    .not_found_err
        mov rdi, err_not_found
    .error:
        call print_error
        jmp .complete
