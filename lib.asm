%define EXIT 60
%define WRITE  1
%define STDOUT  1
%define DIV16TO10  10
%define SPACE  0x20
%define TAB  0x9
%define NXTSTR 0xA

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
		cmp byte[rdi+rax],0
		je .end
		inc rax
		jmp .loop

	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    push rdi
	call string_length
	pop rdi
    mov rsi, rdi
	mov rdx, rax
	mov rax, WRITE
	mov rdi, STDOUT
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rsi, rsp
	pop rdi
	mov rax, WRITE
	mov rdx, 1
	mov rdi, STDOUT
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
	mov r8, DIV16TO10
	mov rax, rdi
	push 0

	.loop:
		xor rdx, rdx
		div r8
		add rdx, '0'
		push rdx
		test rax, rax
		jnz .loop

	.number:
		pop rdi
		test rdi, rdi
		jne .print
		pop r8
		ret

	.print:
		call print_char
		jmp .number

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	xor rax, rax
    test rdi, rdi
	jns .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.print:
		jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rax, rax
    xor rcx, rcx

	.loop:
		mov dl, byte[rdi+rcx]
		cmp dl, byte[rsi+rcx]
		jne .end
		inc rcx
		test dl, dl
		jne .loop
		inc rax

	.end:
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
	mov rdx, 1
	push rax
	mov rsi, rsp
	syscall
	pop rax
	ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	push rdi
	mov r12, rdi
	mov r13, rsi
	mov r14, rcx

	.loop_for_byaka:
		call read_char

		cmp rax, SPACE
		je .loop_for_byaka

		cmp rax, TAB
		je .loop_for_byaka

		cmp rax, NXTSTR
		je .loop_for_byaka
		
		xor rcx, rcx

	.loop:
		inc r14
		cmp r14, r13
		jg .overflow
		mov byte[r12], al

		test rax, rax
		jz .end

		cmp rax, SPACE
		je .end

		cmp rax, TAB
		je .end

		cmp rax, NXTSTR
		je .end

		inc r12
		call read_char

		jmp .loop

	.overflow:
		mov rcx, r14
		dec rcx
		pop rdi
		xor rax, rax
		jmp .finish

	.end:
		mov rdx, r14
		dec rdx
		pop rax

	.finish:
		pop r14
		pop r13
		pop r12
		ret

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rdx, rdx
	xor rcx, rcx
	xor rsi, rsi
	mov r9, DIV16TO10

	.loop:
		mov cl, byte[rdi+rsi]
		xor cl, "0"
		cmp cl, 9
		ja .break
		mul r9
		add rax, rcx
		inc rsi
		jmp .loop
	
	.break:
		mov rdx, rsi
		ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	xor rdx, rdx
	mov cl, byte[rdi]
	cmp cl, "-"
	jne parse_uint
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax

	.loop:
		
		mov cl, byte[rdi+rax]
		mov byte[rsi+rax], cl
		cmp rax, rdx
		jae .end
		inc rax
		test cl, cl
		jne .loop
		ret

	.end:
		xor rax, rax
		ret
