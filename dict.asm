%include "lib.inc"

%define NEXT_ELEM 8

global find_word

section .text

find_word:
    push r12
    push r13
    mov r12, rdi
    mov r13, rsi
    .loop:
        mov rdi, r12
        mov rsi, r13
        add rsi, NEXT_ELEM
        call string_equals
        test rax, rax 
        jnz .success 
        mov r13, [r13] 
        test r13, r13 
        jnz  .loop 
    .failure:
        xor rax, rax
        jmp .exit
    .success:
        mov rax, r13
    .exit:
        pop r13
        pop r12
        ret
